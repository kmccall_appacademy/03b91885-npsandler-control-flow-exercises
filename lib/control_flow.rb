# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  arr = str.chars
  arr.each { |char| arr.delete(char) if char = char.downcase}
  arr.join
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  if str.length % 2 == 0
    str[(str.length / 2 - 1)..(str.length / 2)]
  else
      str[str.length / 2]
  end
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  result = 0
  str.chars.each { |char| result += 1 if VOWELS.include?(char) }
  result
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  result = 1
  while num > 0
    result *= num
    num -= 1
  end
  result
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  result = ""
  return "" if arr == []
  arr[0..-2].each { |el| result << el + separator }
  result << arr[-1]
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  arr = str.split('')
  arr.each_with_index do |char, i|
    if i % 2 == 0
      char.downcase!
    else
      char.upcase!
    end
  end
  arr.join('')
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  str.split(' ').each { |word| word.reverse! if word.length >= 5 }.join(' ')
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  arr = []
  (1..n).each do |num|
    if num % 5 == 0 && num % 3 == 0
      arr << "fizzbuzz"
    elsif num % 5 == 0
      arr << "buzz"
    elsif num % 3 == 0
      arr << "fizz"
    else
      arr << num
    end
  end
  arr
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  arr.reverse
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  return false if num == 1
  divisor = num - 1
  while divisor > 1
    return false if num % divisor == 0
    divisor -= 1
  end
  return true
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  result = []
  (1..num).each { |el| result << el if (num/el) * el == num }
  result
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  arr = factors(num)
  arr.select { |factor| prime?(factor) }
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).length
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  odd = []
  even = []
  arr.each do |el|
    if el % 2 == 0
      even << el
    else
      odd << el
    end
  end
  return odd[0] if odd.length == 1
  return even[0] if even.length == 1
end
